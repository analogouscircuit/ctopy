#include <stdlib.h>
#include "ctopy.h"

/* Main fuctions just for testing.  Will be removed or turned into explicit test
 * procedure */
int main()
{
	double * some_data = malloc(sizeof(double)*10);
	double * more_data = malloc(sizeof(double)*10);
	pydata_list * pdl = pydata_list_init();

	for(int i = 0; i < 10; i++) {
		some_data[i] = i;
		more_data[i] = 2.0*i;
	}

	// Add the data to our list
	int dims1[1] =  {10};
	pydata_list_push(some_data, "double", 
					 1, dims1,
					 "some_doubles", pdl);

	int dims2[2] = {2,5};
	pydata_list_push(more_data, "double", 
					 2, dims2,
					 "more_doubles", pdl);

	// Write the data
	pydata_write(pdl, "my_data");

	// Clean up (note that list takes responsibility for data its given)
	pydata_list_free(pdl);

	return 0;
}
