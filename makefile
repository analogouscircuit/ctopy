CC = gcc
CFLAGS = -g -Wall -O3
LIBS = 

default: ctopy_test

ctopy_test: ctopy_test.o ctopy.o
	$(CC) -o ctopy_test ctopy_test.o ctopy.o $(LIBS)

ctopy_test.o: ctopy_test.c
	$(CC) $(CFLAGS) $(LIBS) -c ctopy_test.c

ctopy.o: ctopy.c ctopy.h
	$(CC) $(CFLAGS) $(LIBS) -c ctopy.c

clean:
	$(RM) ctopy_test
